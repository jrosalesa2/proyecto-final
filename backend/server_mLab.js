require('dotenv').config();
const express = require('express');
const body_parser = require('body-parser');
const request_json = require('request-json');
const app = express();
const port = process.env.PORT || 3000;
const usersFile = require('./users.json');
const URL_BASE = '/techu/v2/';
const URL_mLab = 'https://api.mlab.com/api/1/databases/techu16db/collections/';
const apikey_mlab = 'apiKey='+process.env.API_KEY_MLAB;
const cors = require('cors');  // Instalar dependencia 'cors' con npm

app.listen(port,function(){
console.log('Node JS escuchando en el puerto:' + port);
});
app.use(cors());
app.options('*', cors());

app.use(body_parser.json());

  //Operacion get de accounts con mlab
       app.get(URL_BASE +'users/:id/accounts',
       function(req, res){
         let id = req.params.id;
         const http_client = request_json.createClient(URL_mLab);
         console.log("Cliente http creado correctamente ");
         let field_param = 'f={"_id":0}&';
         let queryString = 'q={"id_user":'+id+'}&';
         http_client.get('user?'+ field_param+queryString +apikey_mlab,
         function(err, respuesta_Mlab, body){
           console.log('Error:'+err);
           console.log('respuesta_Mlab:'+respuesta_Mlab);
           console.log('Body:'+body);
           var response ={};
           if (err) {
             response = {"msg" : "error al recuperar users de mlab"}
             res.status(500);
           }else {
             if (body.length>0) {
               response=body;
             } else {
               response = {"msg" : "usuario no encontrado"}
               res.status(404);
             }
           }
           res.send(response);
         });
   });


//Method POST login
app.post(URL_BASE + "login",
  function (req, res){
    console.log("POST /techu/v2/login");
    let email = req.body.email;
    let pass = req.body.password;
    let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
    let limFilter = 'l=1&';
    let clienteMlab = request_json.createClient(URL_mLab);
    clienteMlab.get('user?'+ queryString + limFilter + apikey_mlab,
      function(error, respuestaMLab, body) {
        if(!error) {
          if (body.length == 1) { // Existe un usuario que cumple 'queryString'
            let login = '{"$set":{"logged":true}}';
            clienteMlab.put('user?q={"id": ' + body[0].id + '}&' + apikey_mlab, JSON.parse(login),
            //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
              function(errPut, resPut, bodyPut) {
                res.send({'msg':'Login correcto', 'user':body[0].email, 'userid':body[0].id_user, 'name':body[0].first_name});
                // If bodyPut.n == 1, put de mLab correcto
              });
          }
          else {
            res.status(404).send({"msg":"Usuario no válido."});
          }
        } else {
          res.status(500).send({"msg": "Error en petición a mLab."});
        }
    });
});

//Method POST logout
app.post(URL_BASE + "logout",
  function(req, res) {
    console.log("POST /techu/v2/logout");
    var email = req.body.email;
    var queryString = 'q={"email":"' + email + '","logged":true}&';
    console.log(queryString);
    var  clienteMlab = request_json.createClient(URL_mLab);
    clienteMlab.get('user?'+ queryString + apikey_mlab,
      function(error, respuestaMLab, body) {
        var respuesta = body[0]; // Asegurar único usuario
        if(!error) {
          console.log("respuesta:"+ respuesta);
          if (respuesta != undefined) { // Existe un usuario que cumple 'queryString'
            let logout = '{"$unset":{"logged":true}}';
            clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apikey_mlab, JSON.parse(logout),
            //clienteMlab.put('user/' + respuesta._id.$oid + '?' + apikeyMLab, JSON.parse(logout),
              function(errPut, resPut, bodyPut) {
                res.send({'msg':'Logout correcto', 'user':respuesta.email});
                // If bodyPut.n == 1, put de mLab correcto
              });
            } else {
                res.status(404).send({"msg":"Logout failed!"});
            }
        } else {
          res.status(500).send({"msg": "Error en petición a mLab."});
        }
    });
});

//GET CON users
app.get(URL_BASE + 'users',
  function(req, res) {
    console.log("entro.");
    const httpClient = request_json.createClient(URL_mLab);
    console.log("Cliente HTTP mLab creado.");
    const fieldParam = 'f={"_id":0}&';
		//console.log('url:' +URL_mLab+'user?' + fieldParam+ apikey_mlab);
    httpClient.get('user?' + fieldParam+ apikey_mlab,
      function(err, respuestaMLab, body) {
        console.log('Error: ' + err);
        console.log('Respuesta MLab: ' + apikey_mlab);
      //  console.log('Body: ' + body);
        var response = {};
        if(err) {
            response = {"msg" : "Error al recuperar users de mLab."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."};
            res.status(404);
          }
        }
        res.send(response);
      });
});

//POST CON users
app.post(URL_BASE+'users',
    function(req, res){
      console.log('entroooooo');
      const http_client = request_json.createClient(URL_mLab);
      let count_param = 'c=true';
      http_client.get(`user?${count_param}&${apikey_mlab}`,
        function(error, res_mlab, count){
          let newId = count + 1;
          let newUser = {
              "id_user": newId,
              "first_name": req.body.first_name,
              "last_name": req.body.last_name,
              "email": req.body.email,
              "password": req.body.password
          };
          console.log('entro 1');
          http_client.post(`user?&${apikey_mlab}`, newUser,
            function(error, res_mlab, body){
              console.log('entro 2');
              res.status(201).send(body);
            });
        });
    }
)

