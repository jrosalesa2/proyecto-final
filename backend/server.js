require('dotenv').config();
const express = require('express');
const body_parser = require('body-parser');
const app = express();
const port = process.env.PORT || 3000;
const URL_BASE = '/techu/v1/';
const usersFile = require('./users.json');

app.listen(port,function(){
console.log('Node JS escuchando en el puerto:' + port);
});

app.use(body_parser.json());

//GET
app.get(URL_BASE + 'users',
function(request, response){
	response.send(usersFile);
});


//Petición GET con un único ID
app.get(URL_BASE + 'users/:id',
function(request, response){
  console.log(request.params.id);
  let pos = request.params.id - 1;
  let respuesta = (usersFile[pos]==undefined)?{"msg":"usuario no existente"}:usersFile[pos];
let status_cod = (usersFile[pos]== undefined) ? 404: 200;
  response.status(status_cod).send(respuesta);
});

//GET con query string
app.get(URL_BASE + 'usersq',
function(request, response){
  console.log(request.query.id);
  response.send({"msg":"GET con query"});
});

//POST
app.post(URL_BASE + 'users',
function(req, res){
        console.log('POST a users');
	let tam =usersFile.length;
let new_user = {
	//"userId":tam,
	"id":tam + 1,
	"first_name":req.body.first_name,
	"last_name":req.body.last_name,
	"email":req.body.email
};
	console.log(new_user);
});

//PUT
app.put(URL_BASE + 'users/:id',
function(req, res){
     console.log('PUT a users');
	let tam =usersFIle.length;
let new_user = {
//"userId":tam,
"id":req.params.id,
"nombre":req.body.nombre,
"email":req.body.email
};
let pos = request.params.id - 1;
usersFile[pos] = new_user;
console.log(new_user);
});

//ELIMINAR
app.delete(URL_BASE + 'users/:id',
function(req, res){
     console.log('REMOVE a users');
     let pos = request.params.id - 1;
usersFile.slice(pos, 1);
console.log("el usuario se ha eliminado correctamente.");
});

//LOGIN
app.post(URL_BASE + 'login',
  function(request, response) {
    console.log("POST /techu/v1/login");
    console.log(request.body.email);
    console.log(request.body.password);
    var user = request.body.email;
    var pass = request.body.password;
    for(us of usersFile) {
      if(us.email == user) {
        if(us.password == pass) {
          us.logged = true;
          writeUserDataToFile(usersFile);
          console.log("Login correcto!");
          response.send({"msg" : "Login correcto.", "idUsuario" : us.id, "logged" : "true"});
        } else {
          console.log("Login incorrecto.");
          response.send({"msg" : "Login incorrecto."});
        }
      }
    }
});

//LOGOUT
app.post(URL_BASE + 'logout',
  function(request, response) {
    console.log("POST /techu/v1/logout");
    var userId = request.body.id;
    for(us of usersFile) {
      if(us.id == userId) {
        if(us.logged) {
          delete us.logged; // borramos propiedad 'logged'
          writeUserDataToFile(usersFile);
          console.log("Logout correcto!");
          response.send({"msg" : "Logout correcto.", "idUsuario" : us.id});
        } else {
          console.log("Logout incorrecto.");
          response.send({"msg" : "Logout incorrecto."});
        }
      }  //us.logged = true
    }
});

//MODIFICAR EL ARCHIVO JS
function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./users.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
     if(err) {
       console.log(err);
     } else {
       console.log("Datos escritos en 'users.json'.");
     }
   }); }
